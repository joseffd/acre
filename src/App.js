import React, { Component } from "react";
import { ApolloClient, ApolloProvider, InMemoryCache } from "@apollo/client";
import { useQuery, gql } from "@apollo/client";
import logo from "./acre-logo.svg";
import "./App.css";

const client = new ApolloClient({
  uri: "http://localhost:4000/graphql",
  cache: new InMemoryCache(),
});
const CLIENTS = gql`
  query Clients($role: String!) {
    users(role: $role) {
      name
      permsissions {
        createCustomer
      }
    }
  }
`;

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { role: "ADMIN", canCreateCustomer: false };
    this.setState = this.setState.bind(this);
    this.handleDrop = this.handleDrop.bind(this);
    this.handleTick = this.handleTick.bind(this);
  }

  handleDrop(event) {
    this.setState({ role: event.target.value });
  }
  handleTick(event) {
    this.setState({ canCreateCustomer: event.target.checked });
  }

  render() {
    return (
      <ApolloProvider client={client}>
        <div className="app">
          <header className="app-header">
            <img src={logo} className="app-logo" alt="logo" />
            <h1>Welcome to acre</h1>
            <h2>Users</h2>
            <select
              name={"role"}
              value={this.state.role}
              onChange={this.handleDrop}
            >
              <option value="ADMIN">Admin</option>
              <option value="BROKER">Broker</option>
              <option value="ADVISOR">Advisor</option>
            </select>
            <label>
              Filter by can create customer:
              <input
                name={"canCreateCustomer"}
                type={"checkbox"}
                checked={this.state.canCreateCustomer}
                onChange={this.handleTick}
              />
            </label>

            <div>
              <GetNames
                role={this.state.role}
                canCreateCustomer={this.state.canCreateCustomer}
              />
            </div>
          </header>
        </div>
      </ApolloProvider>
    );
  }
}

function GetNames({ role, canCreateCustomer }) {
  const { loading, data } = useQuery(CLIENTS, { variables: { role } });
  return (
    <ul>
      {loading
        ? ""
        : data.users.map((user) => filterName(user, canCreateCustomer))}
    </ul>
  );
}

function filterName(user, canCreateCustomer) {
  if (user.name === "") return null;
  if (canCreateCustomer && !user.permsissions.createCustomer) return null;
  if (canCreateCustomer) {
    return (
      <li key={user.name}>
        <div className={"name"}>{user.name}</div>
        <button>Create</button>
      </li>
    );
  }
  return <li key={user.name}>{user.name}</li>;
}

export default App;
